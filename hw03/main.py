from _cooking_food import CookingFood
from _cooking import Cooking, CookingPizza, CookingBread


def main(count: int):
    """Main program functionality."""
    pizza = CookingFood(count, CookingPizza())
    print(pizza.make_food)

    brad = CookingFood(count, CookingBread())
    print(brad.make_food)

    pizza.print_all_ingredients()
    Cooking.print_static_method(count)

    print('flour is ', pizza.flour)
    print('water is ', pizza.water)
    print('cheese is ', pizza.cheese)

    print(pizza.__repr__())
    print(pizza.__str__())
    print('The length of arguments is ', pizza.__len__())


if __name__ == '__main__':
    count = 3
    main(count)
