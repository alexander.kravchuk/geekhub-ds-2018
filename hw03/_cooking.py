"""Cooking food."""


def stock_decorator(method_to_decorate):
    def wrapper(self, count):
        count = count * 2
        print('We have an special offer. Buy one food - get two. You\'ll get %s foods.' % count)
        return method_to_decorate(self, count)

    return wrapper


class Cooking:
    """Class can make a food from ingredients."""
    def __init__(self):
        self._flour = 5
        self._water = 3
        self._cheese = 2

    def __repr__(self):
        return '__repr__ ingredients ({}, {}, {})'.format(self._flour, self._water, self._cheese)

    def __str__(self):
        return '__str__ ingredients ({}, {}, {})'.format(self._flour, self._water, self._cheese)

    def __len__(self):
        return len(self.__dict__)

    def cook(self, count):
        pass

    @staticmethod
    def print_static_method(param):
        print('print_static_method called with ', param)

    @classmethod
    def print_all_ingredients(cls):
        print('print_all_ingredients flour: ', cls.flour, 'water: ', cls.water, 'cheese: ', cls.cheese)

    @property
    def flour(self):
        return self._flour

    @property
    def water(self):
        return self._water

    @property
    def cheese(self):
        return self._cheese


class CookingPizza(Cooking):
    @stock_decorator
    def cook(self, count: int)-> str:
        """Cooking pizza."""
        result = ''
        if (count < 0):
            result = 'Set valid count of pizza'
        else:
            flour = self._flour - count * 0.5
            water = self._water - count * 0.5
            cheese = self._cheese - count * 0.2
            if (flour < 0 or water < 0 or cheese < 0):
                result = 'We don\'t have enough ingredients for making pizza'
            else:
                self._flour -= count * 0.5
                self._water -= count * 0.2
                self._cheese -= count * 0.2
                result = 'Here is (are) your ' + str(count) + ' pizza(s)'

        return result


class CookingBread(Cooking):
    @stock_decorator
    def cook(self, count: int)-> str:
        """Cooking bread."""
        result = ''
        if (count < 0):
            result = 'Set valid count of pizza'
        else:
            flour = self._flour - count * 1
            water = self._water - count * 0.3
            if (flour < 0 or water < 0):
                result = 'We don\'t have enough ingredients for making bread.'
            else:
                self._flour -= count * 1
                self._water -= count * 0.3
                result = 'Here is (are) your ' + str(count) + ' bread(s).'

        return result
