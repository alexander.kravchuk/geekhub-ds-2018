"""Contains classes for cooking food."""


class CookingFood:
    """Class-wrapper for cooking food."""
    def __init__(self, count: int, some_сlass):
        self.make_food = some_сlass.cook(count)

        self.print_all_ingredients = some_сlass.print_all_ingredients
        self.print_static_method = some_сlass.print_static_method

        self.flour = some_сlass.flour
        self.water = some_сlass.water
        self.cheese = some_сlass.cheese

        self.__repr__ = some_сlass.__repr__
        self.__str__ = some_сlass.__str__
        self.__len__ = some_сlass.__len__
