import re

text = "Все йде, все минає і краю немає, \n Куди ж воно ділось? відкіля взялось! \n І дурень, і мудрий нічого не знає. \n Живе... умирає... одно зацвіло, \n А друге зав’яло, навіки зав’яло... \n І листя пожовкле вітри рознесли."
print(text)

# Task 1_1
wordsWithComma = text.split()
print(wordsWithComma)

# Task 1_2
wordsWithoutComma = text.replace(',','').split()
print(wordsWithoutComma)

# Task 1_3
uniqueWords = list(set(text.replace(',','').split()))
print(uniqueWords)

# Task 1_4
reg = re.compile('[^а-яА-Я ]')
print(reg.sub('', text))


# Task 3
def sequence_index(sequence):
    max_len = 0
    length = 0
    for i in range(len(sequence)):
        if sequence[i] == 1:
            length = length + 1
        else:
            if length > max_len:
                max_len = length
                last_index = i - 1
            length = 0
    return last_index - max_len + 1, last_index, max_len


sequence = [1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0]
print('first index, last index, length', sequence_index(sequence))


# Task 4
def max_rectangle_area(histogram):
    stack = []
    top = lambda: stack[-1]
    max_area = 0
    pos = 0
    for pos, height in enumerate(histogram):
        start = pos
        while True:
            if not stack or height > top().height:
                stack.append(Info(start, height))
            elif stack and height < top().height:
                max_area = max(max_area, top().height*(pos-top().start))
                start, _ = stack.pop()
                continue
            break

    pos += 1
    for start, height in stack:
        max_area = max(max_area, height*(pos-start))

    return max_area

print(max_rectangle_area([1, 1, 4, 1]))
print(max_rectangle_area([2, 1, 4, 5, 1, 3, 3]))