#!/usr/bin/env python
# coding: utf-8

# In[2]:


get_ipython().run_line_magic('matplotlib', 'inline')
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = (10, 6)


# In[3]:


players = pd.read_csv('player_data.csv')
players.head(10)


# In[4]:


players.describe(include='all')


# In[5]:


players['weight'].hist(bins=15);


# In[73]:


players.dropna()
new_df = players.where((pd.notnull(players)), 0)
pd.to_numeric(new_df['weight'])
new_df.describe(include='all')
sns.distplot(new_df['weight'], kde=True);


# In[74]:


def weight_category(weight):
    return 'heavier' if weight > 240            else 'lighter' if weight < 180 else 'median'

new_df['weight'] = new_df['weight'].apply(weight_category)
sns.boxplot(x='weight', y='year_start', data=new_df);


# In[75]:


sns.set_palette(sns.color_palette("RdBu"))
sns.pairplot(players[['year_start', 'year_end', 'weight']]);


# In[78]:


stats = pd.read_csv('Seasons_Stats.csv')
stats.head(10)


# In[81]:


stats['Age'].hist(bins=15);


# In[90]:


sns.set_palette(sns.color_palette("RdBu"))
sns.pairplot(stats[['Age', 'Year', 'G', 'MP', 'TS%']]);


# In[ ]:




