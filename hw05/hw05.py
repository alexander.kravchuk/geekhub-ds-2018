import numpy as np
print(np.__version__)

url = 'https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data'

iris = np.genfromtxt(url, delimiter=',', dtype='object')

names = ('sepallength', 'sepalwidth', 'petallength', 'petalwidth', 'species')

data = iris

arr = np.array(data)
print(arr)

species = arr[:,4]
print('species', species)

first_four_col = arr[:,0:4]
print('first_four_col', first_four_col)

arr2d = np.array(first_four_col, float)
print('arr2d', arr2d)
print('Num Dimensions: ', arr2d.ndim)

first_col = arr2d[:,0]

print('first_col', first_col)
print('mean', first_col.mean())
print('median', np.median(first_col))
print('standard', first_col.std())
print('deviation', np.std(first_col, axis=0))

for i in range(20):
    j = np.random.randint(0, 4)
    k = np.random.randint(0, 4)
    arr2d[j, k] = np.nan

print('arr2d', arr2d)

ind = np.where(np.isnan(arr2d[:,0]))
print('Indexes of NaN if first col ', ind)

arr_after_logical_and = arr2d[np.logical_and(arr2d[:,2] > 1.5, arr2d[:,0] < 5)]
print('arr_after_logical_and', arr_after_logical_and)

nan_to_zero = np.nan_to_num(arr_after_logical_and)
print('nan_to_zero', nan_to_zero)

unique_arr = np.unique(nan_to_zero, axis=0)
print('unique_arr', unique_arr, unique_arr.size)

two_hsplit_arr = np.hsplit(unique_arr, 2)
print('two_hsplit_arr', two_hsplit_arr)

first_sorted_arr = two_hsplit_arr[0][np.argsort(two_hsplit_arr[0][:, 0])]
second_sorted_arr = two_hsplit_arr[1][np.argsort(two_hsplit_arr[1][:, 0])]
second_sorted_arr = second_sorted_arr[::-1]
print('first_sorted_arr', first_sorted_arr)
print('second_sorted_arr', second_sorted_arr)

new_arr = np.dstack((first_sorted_arr, second_sorted_arr))
new_arr = new_arr.reshape((first_sorted_arr.shape[0], -1))
print('new_arr', new_arr)


unique,pos = np.unique(new_arr,return_inverse=True)
counts = np.bincount(pos)
maxpos = counts.argmax()
print('common number and count of the common number', unique[maxpos], counts[maxpos])


def custom_function(arr):
    col = 2
    mean_val = arr[:,col].mean()
    print('mean_val', mean_val)
    for i in range(arr[:,col].size):
        if arr[i][col] < mean_val:
            arr[i][col] = arr[i][col] * 2
        else:
            arr[i][col] = arr[i][col] / 4

    return arr


print('result of custom_function work', custom_function(new_arr))
