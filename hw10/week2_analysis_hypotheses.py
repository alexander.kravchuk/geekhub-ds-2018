#!/usr/bin/env python
# coding: utf-8

# <center>
# <img src="https://habrastorage.org/web/677/8e1/337/6778e1337c3d4b159d7e99df94227cb2.jpg"/>
# ## Специализация "Машинное обучение и анализ данных"
# <center>Автор материала: программист-исследователь Mail.Ru Group, старший преподаватель Факультета Компьютерных Наук ВШЭ [Юрий Кашницкий](https://yorko.github.io/)

# # <center> Capstone проект №1 <br>Идентификация пользователей по посещенным веб-страницам
# <img src='http://i.istockimg.com/file_thumbview_approve/21546327/5/stock-illustration-21546327-identification-de-l-utilisateur.jpg'>
# 
# # <center>Неделя 2. Подготовка и первичный анализ данных
# 
# На второй неделе мы продолжим подготавливать данные для дальнейшего анализа и построения прогнозных моделей. Конкретно, раньше мы определили что сессия – это последовательность из 10 посещенных пользователем сайтов, теперь сделаем длину сессии параметром, и потом при обучении прогнозных моделей выберем лучшую длину сессии.
# Также мы познакомимся с предобработанными данными и статистически проверим первые гипотезы, связанные с нашими наблюдениями. 
# 
# **План 2 недели:**
#  - Часть 1. Подготовка нескольких обучающих выборок для сравнения
#  - Часть 2. Первичный анализ данных, проверка гипотез
# 
# **В этой части проекта Вам могут быть полезны  следующие видеозаписи лекций курса "Построение выводов по данным":**
# 
#    - [Доверительные интервалы для доли](https://www.coursera.org/learn/stats-for-data-analysis/lecture/3oi53/dovieritiel-nyie-intiervaly-dlia-doli)
#    - [Биномиальный критерий для доли](https://www.coursera.org/learn/stats-for-data-analysis/lecture/JwmBw/binomial-nyi-kritierii-dlia-doli)
#    - [Доверительные интервалы на основе бутстрепа](https://www.coursera.org/learn/stats-for-data-analysis/lecture/GZjW7/dovieritiel-nyie-intiervaly-na-osnovie-butstriepa)
#    
# **Кроме того, в задании будут использоваться библиотеки Python [glob](https://docs.python.org/3/library/glob.html), [pickle](https://docs.python.org/2/library/pickle.html), [itertools](https://docs.python.org/3/library/itertools.html) и класс [csr_matrix](https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.sparse.csr_matrix.html) из scipy.sparse.**

# ### Задание
# 1. Заполните код в этой тетрадке 
# 2. Если вы проходите специализацию Яндеса и МФТИ, пошлите файл с ответами в соответствующем Programming Assignment. <br> Если вы проходите курс ODS, выберите ответы в [веб-форме](https://docs.google.com/forms/d/13ZnT7w7foHD0uw0ynTtj7atdiCGvlltF8ThhbJCvLsc).  
# 

# ## Часть 1. Подготовка нескольких обучающих выборок для сравнения
# 
# Пока мы брали последовательности из 10 сайтов, и это было наобум. Давайте сделаем число сайтов в сессии параметром, чтоб в дальнейшем сравнить модели классификации, обученные на разных выборках – с 5, 7, 10 и 15 сайтами в сессии. Более того, пока мы брали по 10 сайтов подряд, без пересечения. Теперь давайте применим идею скользящего окна – сессии будут перекрываться. 
# 
# **Пример**: для длины сессии 10 и ширины окна 7 файл из 30 записей породит не 3 сессии, как раньше (1-10, 11-20, 21-30), а 5 (1-10, 8-17, 15-24, 22-30, 29-30). При этом в предпоследней сессии будет один ноль, а в последней – 8 нолей.
# 
# Создадим несколько выборок для разных сочетаний параметров длины сессии и ширины окна. Все они представлены в табличке ниже:
# 
# <style type="text/css">
# .tg  {border-collapse:collapse;border-spacing:0;}
# .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
# .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
# </style>
# <table class="tg">
#   <tr>
#     <th class="tg-031e">session_length -&gt;<br>window_size <br></th>
#     <th class="tg-031e">5</th>
#     <th class="tg-031e">7</th>
#     <th class="tg-031e">10</th>
#     <th class="tg-031e">15</th>
#   </tr>
#   <tr>
#     <td class="tg-031e">5</td>
#     <td class="tg-031e">v</td>
#     <td class="tg-031e">v</td>
#     <td class="tg-031e">v</td>
#     <td class="tg-031e">v</td>
#   </tr>
#   <tr>
#     <td class="tg-031e">7</td>
#     <td class="tg-031e"></td>
#     <td class="tg-031e">v</td>
#     <td class="tg-031e">v</td>
#     <td class="tg-031e">v</td>
#   </tr>
#   <tr>
#     <td class="tg-031e">10</td>
#     <td class="tg-031e"></td>
#     <td class="tg-031e"></td>
#     <td class="tg-031e"><font color='green'>v</font></td>
#     <td class="tg-031e">v</td>
#   </tr>
# </table>
# 
# Итого должно получиться 18 разреженных матриц – указанные в таблице 9 сочетаний параметров формирования сессий для выборок из 10 и 150 пользователей. При этом 2 выборки мы уже сделали в прошлой части, они соответствуют сочетанию параметров: session_length=10, window_size=10, которые помечены в таблице выше галочкой зеленого цвета (done).

# Реализуйте функцию *prepare_sparse_train_set_window*.
# 
# Аргументы:
# - *path_to_csv_files* – путь к каталогу с csv-файлами
# - *site_freq_path* – путь к pickle-файлу с частотным словарем, полученным в 1 части проекта
# - *session_length* – длина сессии (параметр)
# - *window_size* – ширина окна (параметр) 
# 
# Функция должна возвращать 2 объекта:
# - разреженную матрицу *X_sparse* (двухмерная Scipy.sparse.csr_matrix), в которой строки соответствуют сессиям из *session_length* сайтов, а *max(site_id)* столбцов – количеству посещений *site_id* в сессии. 
# - вектор *y* (Numpy array) "ответов" в виде ID пользователей, которым принадлежат сессии из *X_sparse*
# 
# Детали:
# - Модифицируйте созданную в 1 части функцию *prepare_train_set*
# - Некоторые сессии могут повторяться – оставьте как есть, не удаляйте дубликаты
# - Замеряйте время выполнения итераций цикла с помощью *time* из *time*, *tqdm* из *tqdm* или с помощью виджета [log_progress](https://github.com/alexanderkuk/log-progress) ([статья](https://habrahabr.ru/post/276725/) о нем на Хабрахабре)
# - 150 файлов из *capstone_websites_data/150users/* должны обрабатываться за несколько секунд (в зависимости от входных параметров). Если дольше – не страшно, но знайте, что функцию можно ускорить. 

# In[4]:


from __future__ import division, print_function
# отключим всякие предупреждения Anaconda
import warnings
warnings.filterwarnings('ignore')
from glob import glob
import os
import pickle
from tqdm import tqdm_notebook
import numpy as np
import pandas as pd
from scipy.sparse import csr_matrix
from scipy import stats
from statsmodels.stats.proportion import proportion_confint
get_ipython().run_line_magic('matplotlib', 'inline')
from matplotlib import pyplot as plt


# In[5]:


# Поменяйте на свой путь к данным
PATH_TO_DATA = '/home/leybal/projects/ds_course/hw10'


# In[6]:


import glob

def prepare_sparse_train_set_window(path_to_csv_files, site_freq_path, session_length=10, window_size=10):
    files = glob.glob(path_to_csv_files + '*.csv')
    files.sort()

    data = pd.read_pickle(site_freq_path)
    site_freq = pd.Series(data)

    train_data = pd.DataFrame()
    for site_index in range(1, session_length + 1):
        train_data['site' + str(site_index)] = []
    train_data['user_id'] = []

    for i in range(len(files)):
        column = {}
        user_id = i + 1
        data = pd.read_csv(files[i])
        df = pd.DataFrame(data)

        steps_count = df['site'].size // window_size
        if df['site'].size < window_size:
            steps_count = 1
        elif df['site'].size % window_size != 0:
            steps_count = steps_count + 1

        for step in range(steps_count):
            for site_index in range(session_length):
                index = step * window_size + site_index
                
                site_id = 0
                if index < df['site'].size:
                    site = df['site'][index]
                    site_id = site_freq[site][0]

                column['site' + str(site_index + 1)] = site_id

            column['user_id'] = user_id
            train_data = train_data.append(column, ignore_index=True)

    x_data, y_data = train_data.iloc[:, :-1].values, train_data.iloc[:, -1].values

    sites_flatten = x_data.flatten()
    result = csr_matrix(([1] * sites_flatten.shape[0],
                         sites_flatten,
                         range(0, sites_flatten.shape[0] + 10, 10)))[:, 1:]

    return result, y_data


# **Примените полученную функцию с параметрами *session_length=5* и *window_size=3* к игрушечному примеру. Убедитесь, что все работает как надо.**

# In[7]:


X_toy_s5_w3, y_s5_w3 = prepare_sparse_train_set_window(PATH_TO_DATA + '/3users/',
                                                       PATH_TO_DATA + '/site_freq_3users.pkl',
                                       session_length=5, window_size=3)


# In[8]:


X_toy_s5_w3.todense()


# In[9]:


y_s5_w3


# **Запустите созданную функцию 16 раз с помощью циклов по числу пользователей num_users (10 или 150), значениям параметра *session_length* (15, 10, 7 или 5) и значениям параметра *window_size* (10, 7 или 5). Сериализуйте все 16 разреженных матриц (обучающие выборки) и векторов (метки целевого класса – ID пользователя) в файлы `X_sparse_{num_users}users_s{session_length}_w{window_size}.pkl` и `y_{num_users}users_s{session_length}_w{window_size}.pkl`.**
# 
# **Чтоб убедиться, что мы все далее будем работать с идентичными объектами, запишите в список *data_lengths* число строк во всех полученных рареженных матрицах (16 значений). Если какие-то будут совпадать, это нормально (можно сообразить, почему).**
# 
# **На моем ноутбуке этот участок кода отработал за 26 секунд, хотя понятно, что все зависит от эффективности реализации функции *prepare_sparse_train_set_window* и мощности используемого железа. И честно говоря, моя первая реализация была намного менее эффективной (34 минуты), так что тут у Вас есть возможность оптимизировать свой код.**

# In[ ]:


get_ipython().run_cell_magic('time', '', "import itertools\n\ndata_lengths = []\n\nfor num_users in [10, 150]:\n    for window_size, session_length in itertools.product([10, 7, 5], [15, 10, 7, 5]):\n        if window_size <= session_length and (window_size, session_length) != (10, 10):\n            X_sparse, y = ''' ВАШ КОД ЗДЕСЬ '''")


# **<font color='red'> Вопрос 1. </font>Сколько всего уникальных значений в списке `data_lengths`?**

# In[9]:


print(data_lengths)
len(set(data_lengths))


# ## Часть 2. Первичный анализ данных, проверка гипотез

# **Считаем в DataFrame подготовленный на 1 неделе файл `train_data_10users.csv`. Далее будем работать с ним.**

# In[10]:


train_df = pd.read_csv(os.path.join(PATH_TO_DATA, 'train_data_10users.csv'), 
                       index_col='session_id')


# In[11]:


train_df.head()


# In[12]:


train_df.info()


# **Распределение целевого класса:**

# In[13]:


train_df['user_id'].value_counts()


# **Посчитаем распределение числа уникальных сайтов в каждой сессии из 10 посещенных подряд сайтов.**

# In[14]:


num_unique_sites = [np.unique(train_df.values[i, :-1]).shape[0] 
                    for i in range(train_df.shape[0])]


# In[15]:


pd.Series(num_unique_sites).value_counts()


# In[16]:


pd.Series(num_unique_sites).hist();


# **Проверьте с помощью QQ-плота и критерия Шапиро-Уилка, что эта величина распределена нормально**

# **<font color='red'> Вопрос 2. </font>Распределено ли нормально число уникальных сайтов в каждой сессии из 10 посещенных подряд сайтов (согласно критерию Шапиро-Уилка)?**

# In[17]:


stats.probplot(num_unique_sites, dist="norm", plot=plt)
plt.show()

stats.shapiro(pd.Series(num_unique_sites).value_counts())


# **Проверьте гипотезу о том, что пользователь хотя бы раз зайдет на сайт, который он уже ранее посетил в сессии из 10 сайтов. Давайте проверим с помощью биномиального критерия для доли, что доля случаев, когда пользователь повторно посетил какой-то сайт (то есть число уникальных сайтов в сессии < 10) велика: больше 95% (обратите внимание, что альтернатива тому, что доля равна 95% –  одностороняя). Ответом на 3 вопрос в тесте будет полученное p-value.**

# **<font color='red'> Вопрос 3. </font>Каково p-value при проверке описанной гипотезы?**

# In[18]:


has_two_similar = (np.array(num_unique_sites) < 10).astype('int')


# In[19]:


pi_val = stats.binom_test(np.count_nonzero(has_two_similar), len(has_two_similar), 0.95)
pi_val


# **<font color='red'> Вопрос 4. </font>Каков 95% доверительный интервал Уилсона для доли случаев, когда пользователь повторно посетил какой-то сайт (из п. 3)?**

# In[20]:


wilson_interval = proportion_confint(np.count_nonzero(has_two_similar), len(has_two_similar), method='wilson')


# In[21]:


print('{} {}'.format(round(wilson_interval[0], 3),
                                   round(wilson_interval[1], 3)))


# **Постройте распределение частоты посещения сайтов (сколько раз тот или иной сайт попадается в выборке) для сайтов, которые были посещены как минимум 1000 раз.**

# In[22]:


data = pd.read_pickle(PATH_TO_DATA + '/site_freq_10users.pkl')
site_freqs_all_sites = pd.Series(data)
site_index = 0
for index, value in site_freqs_all_sites.items():
    if int(value[1]) < 1000:
        site_index = value[0] - 1
        break
        
site_freqs = site_freqs_all_sites[: site_index]
site_freqs


# In[23]:


num_sites = pd.Series()
for index, value in site_freqs.items():
    num_sites.set_value(value[0], value[1])

pd.Series(num_sites).hist()


# In[24]:


stats.probplot(num_sites, dist="norm", plot=plt)
plt.show()

stats.shapiro(num_sites)


# **Постройте 95% доверительный интервал для средней частоты появления сайта в выборке (во всей, уже не только для тех сайтов, что были посещены как минимум 1000 раз) на основе bootstrap. Используйте столько же bootstrap-подвыборок, сколько сайтов оказалось в исходной выборке по 10 пользователям. Берите подвыборки из посчитанного списка частот посещений сайтов – не надо заново считать эти частоты. Учтите, что частоту появления нуля (сайт с индексом 0 появлялся там, где сессии были короче 10 сайтов) включать не надо. Округлите границы интервала до 3 знаков после запятой и запишите через пробел в файл *answer2_5.txt*. Это будет ответом на 5 вопрос теста.**

# **<font color='red'> Вопрос 5. </font>Каков 95% доверительный интервал для средней частоты появления сайта в выборке?**

# In[141]:


def get_bootstrap_samples(data, n_samples, random_seed=17):
    np.random.seed(random_seed)
    indices = np.random.randint(0, len(data), (n_samples, len(data)))
    samples = data[indices]
    return samples


# In[142]:


def stat_intervals(stat, alpha):
    boundaries = np.percentile(stat, 
                 [100 * alpha / 2., 100 * (1 - alpha / 2.)])
    return boundaries


# In[152]:


data = pd.read_pickle(PATH_TO_DATA + '/site_freq_10users.pkl')
site_freqs_all_sites = pd.Series(data)

num_sites_all = np.array([])
for index, value in site_freqs_all_sites.items():
    num_sites_all = np.append(num_sites_all, [value[1]])

sites_mean_scores = [np.mean(sample) 
                       for sample in get_bootstrap_samples(num_sites_all, len(num_sites_all))]

result = stat_intervals(sites_mean_scores, 0.05)
result = [round(result[0], 3), round(result[1], 3)]

print("mean interval",  result)

answer2_5 = open("answer2_5.txt", "w")
answer = str(result[0]) + ' ' + str(result[1])
answer2_5.write(answer)
answer2_5.close()


# ## Пути улучшения
# Что еще можно добавить по второй части проекта:
# - можно дополнительно рассматривать сессии с параметром – длиной сессии по времени. И составить выборки, скажем, для 5-, 10-, 15- и 20-минутных сессий (это как раз пригодится в [соревновании](https://inclass.kaggle.com/c/catch-me-if-you-can-intruder-detection-through-webpage-session-tracking2) Kaggle Inclass)
# - можно провести больше первичного анализа и проверять прочие интересные гипотезы (а больше их появится после создания признаков на следующей неделе)
# 
# На 3 неделе мы займемся визуальным анализом данных и построением признаков.
