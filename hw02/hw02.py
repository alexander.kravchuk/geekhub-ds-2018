#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# Task 1


# In[19]:


def transform(data):
    if data == 0:
        return
    
    thousands = data // 1000
    hundreds = data // 100 % 10
    tens = data // 10 % 10
    ones = data % 10
    roman_hunds = ["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM"]
    roman_tens = ["","X","XX","XXX","XL","L","LX","LXX","LXXX","XC"]
    roman_ones = ["","I","II","III","IV","V","VI","VII","VIII","IX"]
    result = ""
    
    if thousands > 0:
        result = result + "M" * thousands
    
    result = result + roman_hunds[hundreds] + roman_tens[tens] + roman_ones[ones] 
    return result    
    

for i in range(1, 3999):
      print(transform(i))


# In[ ]:


# Task 2


# In[7]:


def spread_out(data):
    if data < 2:
        return 
    
    numbers = [9, 8, 7, 6, 5, 4, 3, 2]
    old_data = data
    result = []
    
    while data > 1:
        for i in range(len(numbers)):
            if data % numbers[i] != 0:
                continue
            elif data % numbers[i] == 0:
                data = data // numbers[i]
                result.append(numbers[i])
                break
        
        if old_data == data:
            result = [0]
            break
        else: old_data = data
                
    
    if len(result) > 1:
        if result[0] > result[-1]:
            result.reverse();
    
    result = ''.join(map(str, result))
    return result
    
    
for i in range(9, 1537):
      print(i, ' -> ', spread_out(i))


# In[7]:


get_ipython().system(u'jupyter nbconvert --to script hw02.ipynb')


# In[ ]:




