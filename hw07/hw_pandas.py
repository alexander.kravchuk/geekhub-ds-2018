#!/usr/bin/env python
# coding: utf-8

# <center>
# <img src="../../img/ods_stickers.jpg">
# ## Открытый курс по машинному обучению
# <center>
# Автор материала: Юрий Кашницкий, программист-исследователь Mail.Ru Group <br> 
# 
# Материал распространяется на условиях лицензии [Creative Commons CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/). Можно использовать в любых целях (редактировать, поправлять и брать за основу), кроме коммерческих, но с обязательным упоминанием автора материала.

# # <center>Домашнее задание № 1 (демо).<br> Анализ данных по доходу населения UCI Adult</center>

# **В задании предлагается с помощью Pandas ответить на несколько вопросов по данным репозитория UCI [Adult](https://archive.ics.uci.edu/ml/datasets/Adult) (качать данные не надо – они уже есть в репозитории).**

# Уникальные значения признаков (больше информации по ссылке выше):
# - age: continuous.
# - workclass: Private, Self-emp-not-inc, Self-emp-inc, Federal-gov, Local-gov, State-gov, Without-pay, Never-worked.
# - fnlwgt: continuous.
# - education: Bachelors, Some-college, 11th, HS-grad, Prof-school, Assoc-acdm, Assoc-voc, 9th, 7th-8th, 12th, Masters, 1st-4th, 10th, Doctorate, 5th-6th, Preschool.
# - education-num: continuous.
# - marital-status: Married-civ-spouse, Divorced, Never-married, Separated, Widowed, Married-spouse-absent, Married-AF-spouse.
# - occupation: Tech-support, Craft-repair, Other-service, Sales, Exec-managerial, Prof-specialty, Handlers-cleaners, Machine-op-inspct, Adm-clerical, Farming-fishing, Transport-moving, Priv-house-serv, Protective-serv, Armed-Forces.
# - relationship: Wife, Own-child, Husband, Not-in-family, Other-relative, Unmarried.
# - race: White, Asian-Pac-Islander, Amer-Indian-Eskimo, Other, Black.
# - sex: Female, Male.
# - capital-gain: continuous.
# - capital-loss: continuous.
# - hours-per-week: continuous.
# - native-country: United-States, Cambodia, England, Puerto-Rico, Canada, Germany, Outlying-US(Guam-USVI-etc), India, Japan, Greece, South, China, Cuba, Iran, Honduras, Philippines, Italy, Poland, Jamaica, Vietnam, Mexico, Portugal, Ireland, France, Dominican-Republic, Laos, Ecuador, Taiwan, Haiti, Columbia, Hungary, Guatemala, Nicaragua, Scotland, Thailand, Yugoslavia, El-Salvador, Trinadad&Tobago, Peru, Hong, Holand-Netherlands.   
# - salary: >50K,<=50K

# In[1]:


import pandas as pd


# In[2]:


data = pd.read_csv('adult.data.csv')
data.head()


# **1. Сколько мужчин и женщин (признак *sex*) представлено в этом наборе данных?**

# In[7]:


# Ваш код здесь
df = pd.DataFrame(data)
print('Male', df[df['sex'] == 'Male'].shape[0])
print('Female', df[df['sex'] == 'Female'].shape[0])


# **2. Каков средний возраст (признак *age*) женщин?**

# In[9]:


# Ваш код здесь
print('Mean age of female', df[df['sex'] == 'Female'][['age']].mean())


# **3. Какова доля граждан Германии (признак *native-country*)?**

# In[10]:


# Ваш код здесь
print('Part of the germany', df[df['native-country'] == 'Germany'].shape[0] / df[['native-country']].shape[0])


# **4-5. Каковы средние значения и среднеквадратичные отклонения возраста тех, кто получает более 50K в год (признак *salary*) и тех, кто получает менее 50K в год? **

# In[12]:


# Ваш код здесь
print('Mean age of people with salary <=50K', df[df['salary'] == '<=50K']['age'].mean())
print('Mean age of people with salary >50K', df[df['salary'] == '>50K']['age'].mean())
print('Std age of people with salary <=50K', df[df['salary'] == '<=50K']['age'].std())
print('Std age of people with salary >50K', df[df['salary'] == '>50K']['age'].std())


# **6. Правда ли, что люди, которые получают больше 50k, имеют как минимум высшее образование? (признак *education – Bachelors, Prof-school, Assoc-acdm, Assoc-voc, Masters* или *Doctorate*)**

# In[15]:


# Ваш код здесь
education_list = ['Bachelors', 'Prof-school', 'Assoc-acdm', 'Assoc-voc', 'Masters', 'Doctorate']
print('Persent of people with salary >50K and higher education',
      df[(df['education'].isin(education_list)) & (df['salary'] == '>50K')].shape[0] / df[df['salary'] == '>50K'].shape[0])


# **7. Выведите статистику возраста для каждой расы (признак *race*) и каждого пола. Используйте *groupby* и *describe*. Найдите таким образом максимальный возраст мужчин расы *Amer-Indian-Eskimo*.**

# In[16]:


# Ваш код здесь
columns_to_show = ['race', 'sex', 'age']
# print('groupby race', df.groupby(['race'])[columns_to_show].describe())
# print('groupby sex', df.groupby(['sex'])[columns_to_show].describe())
df.groupby(['race', 'sex'])[columns_to_show].describe()


# **8. Среди кого больше доля зарабатывающих много (>50K): среди женатых или холостых мужчин (признак *marital-status*)? Женатыми считаем тех, у кого *marital-status* начинается с *Married* (Married-civ-spouse, Married-spouse-absent или Married-AF-spouse), остальных считаем холостыми.**

# In[17]:


# Ваш код здесь
married = ['Married-civ-spouse', 'Married-spouse-absent', 'Married-AF-spouse']
man_with_bigger_salary = df[(df['salary'] == '>50K') & (df['sex'] == 'Male')]
print('married',
      man_with_bigger_salary[(man_with_bigger_salary['marital-status'].isin(married))].shape[0] / man_with_bigger_salary.shape[0])
print('single',
      man_with_bigger_salary[(~man_with_bigger_salary['marital-status'].isin(married))].shape[0] / man_with_bigger_salary.shape[0])


# **9. Какое максимальное число часов человек работает в неделю (признак *hours-per-week*)? Сколько людей работают такое количество часов и каков среди них процент зарабатывающих много?**

# In[18]:


# Ваш код здесь
max_hours = df['hours-per-week'].max()
count_of_people_with_max_hours = df[df['hours-per-week'] == max_hours].shape[0]
count_of_people_with_max_hours_and_reach = df[(df['hours-per-week'] == max_hours) & (df['salary'] == '>50K')].shape[0]
print('max hours-per-week', max_hours)
print('count of people who work', max_hours, 'hrs per week', count_of_people_with_max_hours)
print('percent of reach people with', max_hours, 'hrs per week',
      count_of_people_with_max_hours_and_reach / count_of_people_with_max_hours)


# **10. Посчитайте среднее время работы (*hours-per-week*) зарабатывающих мало и много (*salary*) для каждой страны (*native-country*).**

# In[22]:


# Ваш код здесь
columns_to_show = ['hours-per-week', 'native-country']
# reach = df[df['salary'] == '>50K']
# poor = df[df['salary'] == '<=50K']
# print('reach people work mean() hrs/week', reach.groupby(['native-country'])[columns_to_show].mean())
# print('poor people work mean() hrs/week', poor.groupby(['native-country'])[columns_to_show].mean())

df.groupby(['native-country', 'salary'])[columns_to_show].mean()


# In[ ]:




