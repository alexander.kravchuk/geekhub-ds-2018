import pandas as pd

data = pd.read_csv('adult.data.csv')
print(data.head())

print(data.tail(3))

print(data.dtypes)
print(data.index)

print('describe', data['sex'].describe())

df = pd.DataFrame(data)

# tasks
print('Male', df[df['sex'] == 'Male'].size)
print('Female', df[df['sex'] == 'Female'].size)

print('Mean age of female', df[df['sex'] == 'Female'][['age']].mean())

print('Part of the germany', df[df['native-country'] == 'Germany'].shape[0] / df[['native-country']].shape[0])

print('Mean age of people with salary <=50K', df[df['salary'] == '<=50K']['age'].mean())
print('Mean age of people with salary >50K', df[df['salary'] == '>50K']['age'].mean())
print('Std age of people with salary <=50K', df[df['salary'] == '<=50K']['age'].std())
print('Std age of people with salary >50K', df[df['salary'] == '>50K']['age'].std())

education_list = ['Bachelors', 'Prof-school', 'Assoc-acdm', 'Assoc-voc', 'Masters', 'Doctorate']
print('Persent of people with salary >50K and higher education',
      df[(df['education'].isin(education_list)) & (df['salary'] == '>50K')].size / df[df['salary'] == '>50K'].size)

columns_to_show = ['race', 'sex', 'age']
print('groupby race', df.groupby(['race'])[columns_to_show].describe())
print('groupby sex', df.groupby(['sex'])[columns_to_show].describe())

married = ['Married-civ-spouse', 'Married-spouse-absent', 'Married-AF-spouse']
man_with_bigger_salary = df[(df['salary'] == '>50K') & (df['sex'] == 'Male')]
print(man_with_bigger_salary)
print('married',
      man_with_bigger_salary[(man_with_bigger_salary['marital-status'].isin(married))].size / man_with_bigger_salary.size)
print('single',
      man_with_bigger_salary[(~man_with_bigger_salary['marital-status'].isin(married))].size / man_with_bigger_salary.size)

max_hours = df['hours-per-week'].max()
count_of_people_with_max_hours = df[df['hours-per-week'] == max_hours].size
count_of_people_with_max_hours_and_reach = df[(df['hours-per-week'] == max_hours) & (df['salary'] == '>50K')].size
print('max hours-per-week', max_hours)
print('count of people who work', max_hours, 'hrs per week', count_of_people_with_max_hours)
print('percent of reach people with', max_hours, 'hrs per week',
      count_of_people_with_max_hours_and_reach / count_of_people_with_max_hours)

reach = df[df['salary'] == '>50K']
poor = df[df['salary'] == '<=50K']
columns_to_show = ['hours-per-week', 'native-country']
print('reach people work mean() hrs/week', reach.groupby(['native-country'])[columns_to_show].mean())
print('poor people work mean() hrs/week', poor.groupby(['native-country'])[columns_to_show].mean())
