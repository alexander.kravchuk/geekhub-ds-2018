#!/usr/bin/env python
# coding: utf-8

# In[3]:


import re


# In[6]:


string = 'Some text ЧЧ:ММ:СС 23:00:01 23:59:59 00:00:00 12:30:59'
pattern = '[0-2][0-9]\:[0-5][0-9]\:[0-5][0-9]'
print(re.findall(pattern, string))


# In[7]:


string = 'Номер телефона в формате +38xx-xxx-xx-xx +3806-312-12-09 +3804-428-11-00'
pattern = '\+38\d{2}\-\d{3}\-\d{2}\-\d{2}'
print(re.findall(pattern, string))


# In[8]:


string = 'Дата в формате ДД.ММ.ГГГГ или ДД/ММ/ГГГГ 01.12.2018 31/12/2017 69.13.0000 00.00.0000'
pattern = '[0-3]\d[./]?[0-1]\d[./]?\d{4}'
print(re.findall(pattern, string))


# In[34]:


string = 'Адрес электронной почты blabla@ex.com A_4example@yandex.ru i-lyahov@livejournal.com john.smith@example.com'
pattern = '[a-zA-Z0-9._%-+]+@[a-zA-Z0-9]+.[a-zA-Z]{2,6}'
print(re.findall(pattern, string))
