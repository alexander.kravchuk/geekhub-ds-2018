#!/usr/bin/env python
# coding: utf-8

# # <center> Capstone проект №1. Идентификация пользователей по посещенным веб-страницам
# 
# # <center>Неделя 2. Подготовка и первичный анализ данных

# ## Часть 1. Подготовка нескольких обучающих выборок для сравнения
# 
# Пока мы брали последовательности из 10 сайтов, и это было наобум. Давайте сделаем число сайтов в сессии параметром, чтоб в дальнейшем сравнить модели классификации, обученные на разных выборках – с 5, 7, 10 и 15 сайтами в сессии. Более того, пока мы брали по 10 сайтов подряд, без пересечения. Теперь давайте применим идею скользящего окна – сессии будут перекрываться. 
# 
# **Пример**: для длины сессии 10 и ширины окна 7 файл из 30 записей породит не 3 сессии, как раньше (1-10, 11-20, 21-30), а 5 (1-10, 8-17, 15-24, 22-30, 29-30). При этом в предпоследней сессии будет один ноль, а в последней – 8 нолей.
# 
# Создадим несколько выборок для разных сочетаний параметров длины сессии и ширины окна. Все они представлены в табличке ниже:
# 
# <style type="text/css">
# .tg  {border-collapse:collapse;border-spacing:0;}
# .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
# .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
# </style>
# <table class="tg">
#   <tr>
#     <th class="tg-031e">session_length -&gt;<br>window_size <br></th>
#     <th class="tg-031e">5</th>
#     <th class="tg-031e">7</th>
#     <th class="tg-031e">10</th>
#     <th class="tg-031e">15</th>
#   </tr>
#   <tr>
#     <td class="tg-031e">5</td>
#     <td class="tg-031e">v</td>
#     <td class="tg-031e">v</td>
#     <td class="tg-031e">v</td>
#     <td class="tg-031e">v</td>
#   </tr>
#   <tr>
#     <td class="tg-031e">7</td>
#     <td class="tg-031e"></td>
#     <td class="tg-031e">v</td>
#     <td class="tg-031e">v</td>
#     <td class="tg-031e">v</td>
#   </tr>
#   <tr>
#     <td class="tg-031e">10</td>
#     <td class="tg-031e"></td>
#     <td class="tg-031e"></td>
#     <td class="tg-031e"><font color='green'>v</font></td>
#     <td class="tg-031e">v</td>
#   </tr>
# </table>
# 
# Итого должно получиться 18 разреженных матриц – указанные в таблице 9 сочетаний параметров формирования сессий для выборок из 10 и 150 пользователей. При этом 2 выборки мы уже сделали в прошлой части, они соответствуют сочетанию параметров: session_length=10, window_size=10, которые помечены в таблице выше галочкой зеленого цвета (done).

# Реализуйте функцию *prepare_sparse_train_set_window*.
# 
# Аргументы:
# - *path_to_csv_files* – путь к каталогу с csv-файлами
# - *site_freq_path* – путь к pickle-файлу с частотным словарем, полученным в 1 части проекта
# - *session_length* – длина сессии (параметр)
# - *window_size* – ширина окна (параметр) 
# 
# Функция должна возвращать 2 объекта:
# - разреженную матрицу *X_sparse* (двухмерная Scipy.sparse.csr_matrix), в которой строки соответствуют сессиям из *session_length* сайтов, а *max(site_id)* столбцов – количеству посещений *site_id* в сессии. 
# - вектор *y* (Numpy array) "ответов" в виде ID пользователей, которым принадлежат сессии из *X_sparse*
# 
# Детали:
# - Модифицируйте созданную в 1 части функцию *prepare_train_set*
# - Некоторые сессии могут повторяться – оставьте как есть, не удаляйте дубликаты
# - Замеряйте время выполнения итераций цикла с помощью *time* из *time*, *tqdm* из *tqdm* или с помощью виджета [log_progress](https://github.com/alexanderkuk/log-progress) ([статья](https://habrahabr.ru/post/276725/) о нем на Хабрахабре)
# - 150 файлов из *capstone_websites_data/150users/* должны обрабатываться за несколько секунд (в зависимости от входных параметров). Если дольше – не страшно, но знайте, что функцию можно ускорить. 

# In[1]:


from __future__ import division, print_function
# отключим всякие предупреждения Anaconda
import warnings
warnings.filterwarnings('ignore')
from glob import glob
import os
import pickle
from tqdm import tqdm_notebook
import numpy as np
import pandas as pd
from scipy.sparse import csr_matrix
from scipy import stats
from statsmodels.stats.proportion import proportion_confint
get_ipython().run_line_magic('matplotlib', 'inline')
from matplotlib import pyplot as plt


# In[2]:


# Поменяйте на свой путь к данным
PATH_TO_DATA = '/home/leybal/projects/ds_course/hw09'


# In[20]:


import glob

def prepare_sparse_train_set_window(path_to_csv_files, site_freq_path, session_length=10, window_size=10):
    files = glob.glob(path_to_csv_files + '*.csv')
    files.sort()

    data = pd.read_pickle(site_freq_path)
    site_freq = pd.Series(data)

    train_data = pd.DataFrame()
    for site_index in range(1, session_length + 1):
        train_data['site' + str(site_index)] = []
    train_data['user_id'] = []

    for i in range(len(files)):
        column = {}
        user_id = i + 1
        data = pd.read_csv(files[i])
        df = pd.DataFrame(data)

        steps_count = df['site'].size // window_size
        if df['site'].size < window_size:
            steps_count = 1
        elif df['site'].size % window_size != 0:
            steps_count = steps_count + 1

        for step in range(steps_count):
            for site_index in range(session_length):
                index = step * window_size + site_index
                
                site_id = 0
                if index < df['site'].size:
                    site = df['site'][index]
                    site_id = site_freq[site][0]

                column['site' + str(site_index + 1)] = site_id

            column['user_id'] = user_id
            train_data = train_data.append(column, ignore_index=True)

    x_data, y_data = train_data.iloc[:, :-1].values, train_data.iloc[:, -1].values

    sites_flatten = x_data.flatten()
    result = csr_matrix(([1] * sites_flatten.shape[0],
                         sites_flatten,
                         range(0, sites_flatten.shape[0] + 10, 10)))[:, 1:]

    return result, y_data


# **Примените полученную функцию с параметрами *session_length=5* и *window_size=3* к игрушечному примеру. Убедитесь, что все работает как надо.**

# In[23]:


get_ipython().run_cell_magic('time', '', "X_toy_s5_w3, y_s5_w3 = prepare_sparse_train_set_window(PATH_TO_DATA + '/3users/',\n                                                       PATH_TO_DATA + '/site_freq_3users.pkl',\n                                       session_length=5, window_size=3)")


# In[73]:


X_toy_s5_w3.todense()


# In[24]:


y_s5_w3


# **Запустите созданную функцию 16 раз с помощью циклов по числу пользователей num_users (10 или 150), значениям параметра *session_length* (15, 10, 7 или 5) и значениям параметра *window_size* (10, 7 или 5). Сериализуйте все 16 разреженных матриц (обучающие выборки) и векторов (метки целевого класса – ID пользователя) в файлы `X_sparse_{num_users}users_s{session_length}_w{window_size}.pkl` и `y_{num_users}users_s{session_length}_w{window_size}.pkl`.**
# 
# **Чтоб убедиться, что мы все далее будем работать с идентичными объектами, запишите в список *data_lengths* число строк во всех полученных рареженных матрицах (16 значений). Если какие-то будут совпадать, это нормально (можно сообразить, почему).**
# 
# **На моем ноутбуке этот участок кода отработал за 26 секунд, хотя понятно, что все зависит от эффективности реализации функции *prepare_sparse_train_set_window* и мощности используемого железа. И честно говоря, моя первая реализация была намного менее эффективной (34 минуты), так что тут у Вас есть возможность оптимизировать свой код.**

# In[85]:


get_ipython().run_cell_magic('time', '', "import itertools\n\ndata_lengths = []\n\nfor num_users in [10, 150]:\n    for window_size, session_length in itertools.product([10], [15]):\n        for window_size, session_length in itertools.product([10, 7, 5], [15, 10, 7, 5]):\n            X_sparse, y = prepare_sparse_train_set_window(PATH_TO_DATA + '/' + str(num_users) + 'users/',\n                                                          PATH_TO_DATA + '/site_freq_' + str(num_users) + 'users.pkl',\n                                                          session_length=5, window_size=3)\n            data_lengths.append(X_sparse.todense().shape[0])")


# **<font color='red'> Вопрос 1. </font>Сколько всего уникальных значений в списке `data_lengths`?**

# In[89]:


print(data_lengths)
len(set(data_lengths))


# In[ ]:




