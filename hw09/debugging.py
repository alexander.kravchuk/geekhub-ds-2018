from __future__ import division, print_function
# отключим всякие предупреждения Anaconda
import warnings
warnings.filterwarnings('ignore')
from glob import glob
import os
import pickle
#pip install tqdm
from tqdm import tqdm_notebook
import numpy as np
import pandas as pd
from scipy.sparse import csr_matrix

import glob
from tqdm import tqdm, trange

PATH_TO_DATA = '/home/leybal/projects/ds_course/hw09'


def prepare_train_set(path_to_csv_files, session_length=10):
    ''' ВАШ КОД ЗДЕСЬ '''
    files = glob.glob(path_to_csv_files + '*.csv')
    files.sort()
    sites = pd.Series()
    site_freq = pd.Series()
    train_data = pd.DataFrame({
        'site1': [], 'site2': [], 'site3': [], 'site4': [], 'site5': [], 'site6': [], 'site7': [], 'site8': [],
        'site9': [], 'site10': [],
        'user_id': []
    })

    for i in range(len(files)):
        data = pd.read_csv(files[i])
        df = pd.DataFrame(data)
        for index, row in df.iterrows():
            site = row['site']
            if site not in sites:
                sites[site] = 1
            else:
                sites[site] = sites[site] + 1

    sites = sites.sort_values(ascending=False)
    keys = sites.keys()
    for j in range(sites.size):
        site_freq[keys[j]] = (j + 1, sites[j])

    for i in range(len(files)):
        column = {}
        user_id = i + 1
        data = pd.read_csv(files[i])
        df = pd.DataFrame(data)
        for index, row in df.iterrows():
            site = row['site']

            j = index - index // session_length * session_length + 1
            column['site' + str(j)] = site_freq[site][0]

            if j == session_length:
                column['user_id'] = user_id
                train_data = train_data.append(column, ignore_index=True)

            if (index == df['site'].size - 1) and (j < session_length):
                for k in range(j + 1, session_length + 1):
                    column['site' + str(k)] = 0

                column['user_id'] = user_id
                train_data = train_data.append(column, ignore_index=True)

    return train_data, site_freq


# data, site_freq = prepare_train_set(PATH_TO_DATA + '/3users/', 10)


def prepare_sparse_train_set_window(path_to_csv_files, site_freq_path, session_length=10, window_size=10):
    files = glob.glob(path_to_csv_files + '*.csv')
    files.sort()

    data = pd.read_pickle(site_freq_path)
    site_freq = pd.Series(data)

    train_data = pd.DataFrame()
    for site_index in range(1, session_length + 1):
        train_data['site' + str(site_index)] = []
    train_data['user_id'] = []

    for i in range(len(files)):
        column = {}
        user_id = i + 1
        data = pd.read_csv(files[i])
        df = pd.DataFrame(data)

        steps_count = df['site'].size // window_size
        if df['site'].size < window_size:
            steps_count = 1
        elif df['site'].size % window_size != 0:
            steps_count = steps_count + 1

        for step in range(steps_count):
            for site_index in range(session_length):
                index = step * window_size + site_index

                site_id = 0
                if index < df['site'].size:
                    site = df['site'][index]
                    site_id = site_freq[site][0]

                column['site' + str(site_index + 1)] = site_id

            column['user_id'] = user_id
            train_data = train_data.append(column, ignore_index=True)

    x_data, y_data = train_data.iloc[:, :-1].values, train_data.iloc[:, -1].values

    sites_flatten = x_data.flatten()
    result = csr_matrix(([1] * sites_flatten.shape[0],
                         sites_flatten,
                         range(0, sites_flatten.shape[0] + 10, 10)))[:, 1:]

    return result, y_data

# x, y = prepare_sparse_train_set_window(PATH_TO_DATA + '/3users/', PATH_TO_DATA + '/site_freq_3users.pkl', 10, 7)

import datetime


def prepare_train_set_with_fe(path_to_csv_files, site_freq_path, feature_names, session_length=10, window_size=10):
    files = glob.glob(path_to_csv_files + '*.csv')
    files.sort()

    data = pd.read_pickle(site_freq_path)
    site_freq = pd.Series(data)

    train_data = pd.DataFrame()
    for feature_name in feature_names:
        train_data[feature_name] = []

    for i in range(len(files)):
        column = {}
        user_id = i + 1
        data = pd.read_csv(files[i])
        df = pd.DataFrame(data)

        steps_count = df['site'].size // window_size
        if df['site'].size < window_size:
            steps_count = 1
        elif df['site'].size % window_size != 0:
            steps_count = steps_count + 1

        for step in range(steps_count):
            date_list = []
            for site_index in range(session_length):
                index = step * window_size + site_index

                site_id = 0
                if index < df['site'].size:
                    site = df['site'][index]
                    site_id = site_freq[site][0]

                column['site' + str(site_index + 1)] = site_id

                if (index < df['timestamp'].size - 1) and (site_index < session_length - 1):
                    start = datetime.datetime.strptime(df['timestamp'][index], '%Y-%m-%d %H:%M:%S')
                    finish = datetime.datetime.strptime(df['timestamp'][index + 1], '%Y-%m-%d %H:%M:%S')
                    delta = finish - start
                    column['time_diff' + str(site_index + 1)] = delta.seconds
                    date_list.append(start)
                elif site_index < session_length - 1:
                    column['time_diff' + str(site_index + 1)] = 0

            if len(date_list) != 0:
                column['start_hour'] = min(date_list)
                column['day_of_week'] = column['start_hour'].weekday()
            else:
                column['start_hour'] = 0
                column['day_of_week'] = 0

            column['target'] = user_id
            train_data = train_data.append(column, ignore_index=True)

    for i, row in train_data.iterrows():
        train_data_row = train_data.loc[i]
        date_df = pd.DataFrame(train_data_row['time_diff1': 'time_diff9'])
        train_data['session_timespan'][i] = date_df.max() - date_df.min()
        train_data['#unique_sites'][i] = len(set(train_data_row['site1': 'site10']))

    return train_data

feature_names = ['site' + str(i) for i in range(1,11)] + \
                ['time_diff' + str(j) for j in range(1,10)] + \
                ['session_timespan', '#unique_sites', 'start_hour',
                 'day_of_week', 'target']

# x = prepare_train_set_with_fe(PATH_TO_DATA + '/3users/', PATH_TO_DATA + '/site_freq_3users.pkl', feature_names, 10, 10)

train_data_150users = prepare_train_set_with_fe(PATH_TO_DATA + '/150users/', PATH_TO_DATA + '/site_freq_150users.pkl', feature_names, 10, 10)

new_features_150users = train_data_150users.loc[:,'session_timespan': 'day_of_week']

with open(os.path.join(PATH_TO_DATA, 'new_features_150users.pkl'), 'wb') as new_features_150users_pkl:
    pickle.dump(new_features_150users, new_features_150users_pkl)