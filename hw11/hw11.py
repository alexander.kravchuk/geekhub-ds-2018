#!/usr/bin/env python
# coding: utf-8

# In[122]:


import numpy as np
import pandas as pd
import matplotlib

from enn.enn import ENN
from pylmnn import LargeMarginNearestNeighbor as LMNN
from scipy.spatial.distance import euclidean, mahalanobis
from sklearn.neighbors import DistanceMetric
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score, roc_auc_score
from sklearn.model_selection import cross_val_score, GridSearchCV, train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC


# In[123]:


path='winequality-red.csv'

df=pd.read_csv(path, ';')
df.head()


# In[124]:


df.shape


# In[125]:


df.describe()


# In[126]:


quality=df['quality'].groupby(df['quality']).count()
quality


# In[127]:


quality.plot('bar');


# In[152]:


path='winequality-white.csv'
df_white=pd.read_csv(path, ';')
quality_white=df_white['quality'].groupby(df_white['quality']).count()
quality_white.plot('bar');
df_white.head()


# In[128]:


# get column titles except the last column
features=df.columns[:-1].tolist()

# get data set features
X=df[features].values
# get labels
y=df['quality'].values

# split data to train data set and test data set
X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.2,random_state=1)

scores=[]


# In[130]:


# loop k from 1 to 8, and get cross validation score of each K value
for k in range(1,9):
    knn=KNeighborsClassifier(k)
    score_val=cross_val_score(knn,X_train,y_train,scoring='accuracy',cv=5)
    score_mean=score_val.mean()
    scores.append(score_mean)

scores_df = pd.DataFrame(scores)
scores_df.plot()


# In[133]:


# get index of maxium score along axis, default axis=0 for 1 dimensional array
best_k=np.argmax(scores)+1
print(best_k)
# generate KNN model
knn=KNeighborsClassifier(best_k)
# fit with train data set
knn.fit(X_train,y_train)
# get Modes presicion rate using test set
print("prediction precision rate:",knn.score(X_test,y_test))


# In[134]:


scores=[]
# loop k from 1 to 8, and get cross validation score of each K value
k_test, n_components, max_iter = 5, X.shape[1], 180
for k in range(1,9):
    # Instantiate the metric learner
    lmnn = LMNN(n_neighbors=k, max_iter=max_iter, n_components=n_components)
    # Train the metric learner
    lmnn.fit(X_train, y_train)
    # Fit the nearest neighbors classifier
    knn = KNeighborsClassifier(n_neighbors=k_test)
    knn.fit(lmnn.transform(X_train), y_train)
    score_val=cross_val_score(knn,lmnn.transform(X_train),y_train,scoring='accuracy',cv=5)
    score_mean=score_val.mean()
    scores.append(score_mean)


# In[135]:


# get index of maxium score along axis, default axis=0 for 1 dimensional array
best_k=np.argmax(scores)+1
print(best_k)
# Instantiate the metric learner
lmnn = LMNN(n_neighbors=int(best_k), max_iter=max_iter, n_components=n_components)
# Train the metric learner
lmnn.fit(X_train, y_train)
# generate KNN model
knn=KNeighborsClassifier(n_neighbors=best_k)
# fit with train data set
knn.fit(lmnn.transform(X_train), y_train)
# get Modes presicion rate using test set
print("prediction precision rate:",knn.score(lmnn.transform(X_test),y_test))


# In[138]:


# The implementation is based on libsvm. 
# The fit time complexity is more than quadratic with the number of samples 
# which makes it hard to scale to dataset with more than a couple of 10000 samples.

clf = SVC(gamma='auto')
clf.fit(X_train, y_train)
print("prediction precision rate:",clf.score(X_test,y_test))


# In[140]:


y_pred = clf.predict(X_test)
print(clf)
print('Train scores')
print('accuracy_score = ', accuracy_score(y_test, y_pred))
print('precision_score = ', precision_score(y_test, y_pred, average='macro'))
print('recall_score = ', recall_score(y_test, y_pred, average='macro'))
print('f1_score = ', f1_score(y_test, y_pred, average='macro'))
#print('roc_auc_score = ', roc_auc_score(y_train, y_pred))


# In[141]:


y_pred, y_test


# In[142]:


clf = ENN()
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
print(clf)
print('Train scores')
print('accuracy_score = ', accuracy_score(y_test, y_pred))
print('precision_score = ', precision_score(y_test, y_pred, average='macro'))
print('recall_score = ', recall_score(y_test, y_pred, average='macro'))
print('f1_score = ', f1_score(y_test, y_pred, average='macro'))


# In[143]:


# Улучшение скора
# Get balanced sample by oversampling
df3=df[df['quality']==3]
df4=df[df['quality']==4]
df5=df[df['quality']==5]
df6=df[df['quality']==6]
df7=df[df['quality']==7]
df8=df[df['quality']==8]

df3=pd.concat([df3]*50)
df4=pd.concat([df4]*10)
df7=pd.concat([df7]*3)
df8=pd.concat([df8]*29)

df_balanced=pd.concat([df3,df4,df5,df6,df7,df8])
df_balanced.shape


# In[144]:


type=df_balanced['quality'].groupby(df_balanced['quality']).count()
type


# In[145]:


type.plot('bar')


# In[146]:


# df.columns is column labels property
features=df_balanced.columns[:-1].tolist()
X=df_balanced[features].values
y=df_balanced['quality']

X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.2,random_state=1)


# In[147]:


scores=[]
for i in range(40):
    knn=KNeighborsClassifier(k)
    score_val=cross_val_score(knn,X_train,y_train,scoring='accuracy',cv=10)
    score_mean=score_val.mean()
    scores.append(score_mean)
best_K=np.argmax(scores)+1
print('best K is:',best_K)
knn=KNeighborsClassifier(best_K)
knn.fit(X_train,y_train)
print("prediction precision rate:",knn.score(X_test,y_test))


# In[ ]:




